﻿using System;
using System.IO;
using System.Reflection;

namespace ShowTemperature
{
    internal sealed class Program
    {
        private const string UsageStr =
            "Usage: dotnet showtemp.dll SHOW... [OPTION...]\r\n" +
            "Show temperature of CPU in °C\r\n" +
            "  -u --usage:    show usage info\r\n" +
            "  -v --version:  show version\r\n" +
            "  -c --cpu:      show CPU temperature in °C\r\n" +
            "  -r --raw:      [OPTION] show temperature in raw floating point format instead of formatting output\r\n" +
            "                   example with formatting: \"CPU temperature: 54,8 °C\"\r\n" +
            "                   example without formatting: \"54,768\"";

        private enum ExitCode : int
        {
            Success = 0,
            ArgsErr = 1,
            CpuTempReadErr = 2
        }

        private static int Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine(UsageStr);

                return (int)ExitCode.Success;
            }
                        
            var firstArg = args[0];
            
            if (string.IsNullOrWhiteSpace(firstArg))
            {
                return (int)ExitCode.ArgsErr;
            }
            
            switch (firstArg)
            {
                case "-u":
                case "--u":
                case "-usage":
                case "--usage":
                {
                    Console.WriteLine(UsageStr);

                    return (int)ExitCode.Success;
                }

                case "-v":
                case "--v":
                case "-version":
                case "--version":
                {
                    var assembly = AssemblyName.GetAssemblyName(Assembly.GetExecutingAssembly().Location);

                    Console.WriteLine($"{assembly.Name} version {assembly.Version}");

                    return (int)ExitCode.Success;
                }

                case "-c":
                case "--c":
                case "-cpu":
                case "--cpu":
                    break;
                        
                default:
                {   
                    Console.WriteLine(UsageStr);

                    return (int)ExitCode.ArgsErr;
                }
            }
            
            var raw = false;

            if (args.Length == 2)
            {
                var secondArg = args[1];

                if (!string.IsNullOrWhiteSpace(secondArg))
                {
                    switch(secondArg)
                    {
                        case "-r":
                        case "--r":
                        case "-raw":
                        case "--raw":
                        {
                            raw = true;
                        }
                        break;

                        default:
                        {
                            Console.WriteLine(UsageStr);

                            return (int)ExitCode.ArgsErr;
                        }
                    }
                }
            }

            try
            {
                ShowTemperature(raw);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return (int)ExitCode.CpuTempReadErr;
            }

            return (int)ExitCode.Success;
        }

        private static void ShowTemperature(bool raw)
        {
            var temperature = ReadCpuTemerature();

            if (raw)
            {
                Console.WriteLine(temperature);
            }
            else
            {
                Console.WriteLine("CPU temperature: {0:F1} °C", temperature);
            }
        }

        private static double ReadCpuTemerature()
        {
            var cpuTempRaw = string.Empty;

            try
            {
                cpuTempRaw = File.ReadAllText("/sys/class/thermal/thermal_zone0/temp");
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"CPU temerature read exception:{Environment.NewLine}{ex.Message}", ex);
            }

            try
            {
                return double.Parse(cpuTempRaw) / 1000;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"CPU temperature parse error:{Environment.NewLine}{ex.Message}", ex);
            }
        }
    }
}