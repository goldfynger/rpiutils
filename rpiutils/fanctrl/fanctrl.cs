using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;

namespace fanctrl
{
    /// <summary></summary>
    internal class fanctrl
    {
        /// <summary></summary>
        private const string UsageStr =
            "Usage: mono fanctrl.exe [-h | -u | -v] addr [-s]\r\n" +
            "Print temperature of CPU and GPU\r\n" +
            "\t-h: show usage info\r\n" +
            "\t-u: same as -h\r\n" +
            "\t-v: show version\r\n" +
            "\taddr: address of I2C target, can be in decimal or hex with 0x or 0X format\r\n" +
            "\t-s: suppress info messages";


        /// <summary></summary>
        private enum ExitCode
        {
            Success = 0,
            ArgsLenErr = 1,
            ArgsErr = 2,
            ExecuteErr = 3,
        }


        /// <summary></summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            byte address = 0;
            bool suppressMsg = false;

            if (args.Length == 0)
            {
                ExitWithCode(ExitCode.ArgsLenErr, UsageStr);
            }

            var firstArg = args[0];

            if (string.IsNullOrWhiteSpace(firstArg))
            {
                ExitWithCode(ExitCode.ArgsErr, UsageStr);
            }

            if (firstArg.Length == 2)
            {
                switch (firstArg)
                {
                    case "-h":
                    case "-u":
                        ExitWithCode(ExitCode.Success, UsageStr);
                        break;

                    case "-v":
                        var assembly = AssemblyName.GetAssemblyName(Assembly.GetExecutingAssembly().Location);
                        ExitWithCode(ExitCode.Success, $"{assembly.Name} ver {assembly.Version}");
                        break;
                }
            }

            if (firstArg.Length > 2 && (firstArg.StartsWith("0x") || firstArg.StartsWith("0X")))
            {
                if (!byte.TryParse(firstArg, System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out address))
                {
                    ExitWithCode(ExitCode.ArgsErr, UsageStr);
                }
            }
            else if (!byte.TryParse(firstArg, out address))
            {
                ExitWithCode(ExitCode.ArgsErr, UsageStr);
            }

            if (args.Length == 2)
            {
                if (args[1] != "-s")
                {
                    ExitWithCode(ExitCode.ArgsErr, UsageStr);
                }

                suppressMsg = true;
            }

            if (args.Length > 2)
            {
                ExitWithCode(ExitCode.ArgsLenErr, UsageStr);
            }


            try
            {
                if (!suppressMsg)
                {
                    Console.Write("Get CPU temperature ... ");
                }

                var temperature = GetCpuTemperature();

                if (!suppressMsg)
                {
                    Console.WriteLine(temperature);
                }


                if (!suppressMsg)
                {
                    Console.Write("Send temperature to address 0x{0:X} ... ", address);
                }

                SendTemperature(address, Convert.ToSByte(temperature));

                if (!suppressMsg)
                {
                    Console.WriteLine("OK");
                }
            }
            catch (Exception ex)
            {
                ExitWithCode(ExitCode.ExecuteErr, ex.Message);
            }


            ExitWithCode(ExitCode.Success);
        }

        /// <summary>
        /// Gets the cpu temperature.
        /// </summary>
        private static double GetCpuTemperature()
        {
            using (var tempReadProcess = new Process())
            {
                tempReadProcess.StartInfo.UseShellExecute = false;
                tempReadProcess.StartInfo.RedirectStandardOutput = true;
                tempReadProcess.StartInfo.FileName = "mono";
                var executePath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                tempReadProcess.StartInfo.Arguments = $"{executePath}/shwtmp.exe -c -r";
                tempReadProcess.Start();
                var resultsRaw = tempReadProcess.StandardOutput.ReadToEnd();
                tempReadProcess.WaitForExit();            

                if (tempReadProcess.ExitCode != 0)
                {
                    throw new ApplicationException($"shwtmp return error code {tempReadProcess.ExitCode}");
                }

                return double.Parse(resultsRaw);
            }
        }

        /// <summary>
        /// Sends the temperature.
        /// </summary>
        /// <param name="address">Address.</param>
        /// <param name="value">Value.</param>
        private static void SendTemperature(byte address, sbyte value)
        {
            using (var tempWriteProcess = new Process())
            {
                tempWriteProcess.StartInfo.UseShellExecute = false;
                tempWriteProcess.StartInfo.RedirectStandardOutput = true;
                tempWriteProcess.StartInfo.FileName = "i2cset";
                tempWriteProcess.StartInfo.Arguments = $"-y 1 {address} {value}";
                tempWriteProcess.Start();
                tempWriteProcess.StandardOutput.ReadToEnd();
                tempWriteProcess.WaitForExit();

                if (tempWriteProcess.ExitCode != 0)
                {
                    throw new ApplicationException($"i2cset return error code {tempWriteProcess.ExitCode}");
                }
            }
        }

        /// <summary>
        /// Writes message if need and exits with code.
        /// </summary>
        /// <param name="code">Code.</param>
        /// <param name="message">Message.</param>
        private static void ExitWithCode(ExitCode code, string message = null)
        {
            if (message != null)
            {
                Console.WriteLine(message);
            }

            Environment.Exit((int)code);
        }
    }
}
