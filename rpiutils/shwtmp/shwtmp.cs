﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace shwtmp
{
    /// <summary></summary>
    internal class shwtmp
    {
        /// <summary></summary>
        private const string UsageStr =
            "Usage: mono shwtmp.exe [-h | -u | -v] -cg | -A  [-r]\r\n" +
            "Print temperature of CPU and GPU\r\n" +
            "\t-h: show usage info\r\n" +
            "\t-u: same as -h\r\n" +
            "\t-v: show version\r\n" +
            "\t-c: show CPU temperature\r\n" +
            "\t-g: show GPU temperature\r\n" +
            "\t-cg: show CPU and GPU temperature when CPU must be first\r\n" +
            "\t-A: same as -cg\r\n" +
            "\t-r: show temperature in raw floating point format instead of formatting output\r\n" +
            "\t\tresult with formatting: \"Temperature: [CPU:%cputemp%] [GPU:%gputemp%]\"\r\n" +
            "\t\tresult without formatting: \"[%cputemp%] [%gputemp%]\"";

        
        /// <summary></summary>
        private enum ExitCode
        {
            Success = 0,
            ArgsLenErr = 1,
            ArgsErr = 2,
            CpuTReadErr = 3,
            CpuTParseErr = 4,
            GpuTReadErr = 5,
            GpuTParseErr = 6,
        }

    
        /// <summary></summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            // Parse args.
            bool showCpu = false;
            bool showGpu = false;
            bool showRaw = false;
        
            if (args.Length == 0)
            {
                ExitWithCode(ExitCode.ArgsLenErr, UsageStr);
            }
            
            var firstArg = args[0];
            
            if (string.IsNullOrWhiteSpace(firstArg))
            {
                ExitWithCode(ExitCode.ArgsErr, UsageStr);
            }
            
            switch (firstArg)
            {
                case "-A":
                case "-cg":
                    showCpu = true;
                    showGpu = true;
                    break;

                case "-c":
                    showCpu = true;
                    break;

                case "-g":
                    showGpu = true;
                    break;
                        
                case "-h":
                case "-u":
                    ExitWithCode(ExitCode.Success, UsageStr);
                    break;

                case "-v":
                    var assembly = AssemblyName.GetAssemblyName(Assembly.GetExecutingAssembly().Location);
                    ExitWithCode(ExitCode.Success, $"{assembly.Name} ver {assembly.Version}");
                    break;
                        
                default:
                    ExitWithCode(ExitCode.ArgsErr, UsageStr);
                    break;
            }
            
            if (args.Length == 2)
            {
                if (args[1] != "-r")
                {
                    ExitWithCode(ExitCode.ArgsErr, UsageStr);
                }

                showRaw = true;
            }
            
            if (args.Length > 2)
            {
                ExitWithCode(ExitCode.ArgsLenErr, UsageStr);
            }


            var cpuTemp = 0.0;
            var gpuTemp = 0.0;
            
            
            if (showCpu)
            {
                // Read from /sys/class/thermal/thermal_zone0/temp.
                var cpuTempRaw = string.Empty;

                try
                {
                    cpuTempRaw = File.ReadAllText("/sys/class/thermal/thermal_zone0/temp");
                }
                catch (Exception ex)
                {
                    ExitWithCode(ExitCode.CpuTReadErr, $"CPU temperature read error {ex.Message}");
                }

                try
                {
                    cpuTemp = double.Parse(cpuTempRaw) / 1000;
                }
                catch (Exception ex)
                {
                    ExitWithCode(ExitCode.CpuTParseErr, $"CPU temperature parse error {ex.Message}");
                }
            }

            
            if (showGpu)
            {
                // Execute vcgencmd with measure_temp argument.
                var gpuTempRaw = string.Empty;

                try
                {
                    using (var gpuTempReadProcess = new Process())
                    {
                        gpuTempReadProcess.StartInfo.UseShellExecute = false;
                        gpuTempReadProcess.StartInfo.RedirectStandardOutput = true;
                        gpuTempReadProcess.StartInfo.FileName = "vcgencmd";
                        gpuTempReadProcess.StartInfo.Arguments = "measure_temp";
                        gpuTempReadProcess.Start();
                        gpuTempRaw = gpuTempReadProcess.StandardOutput.ReadToEnd();
                        gpuTempReadProcess.WaitForExit();
                        
                        if (gpuTempReadProcess.ExitCode != 0)
                        {
                            throw new ApplicationException($"vcgencmd return error code {gpuTempReadProcess.ExitCode}");
                        }
                    }

                    if (string.IsNullOrWhiteSpace(gpuTempRaw))
                    {
                        throw new ApplicationException("vcgencmd return wrong string");
                    }
                }
                catch (Exception ex)
                {
                    ExitWithCode(ExitCode.GpuTReadErr, $"GPU temperature read error {ex.Message}");
                }

                try
                {
                    var gpuTempIdxStart = 0;
                    while (gpuTempRaw.Length > gpuTempIdxStart && !char.IsDigit(gpuTempRaw[gpuTempIdxStart]))
                    {
                        gpuTempIdxStart++;
                    }

                    var gpuTempIdxEnd = gpuTempIdxStart;
                    while (gpuTempRaw.Length > gpuTempIdxEnd && gpuTempRaw[gpuTempIdxEnd] != '\'')
                    {
                        gpuTempIdxEnd++;
                    }

                    if (gpuTempIdxStart == gpuTempRaw.Length - 1 || gpuTempIdxEnd == gpuTempRaw.Length - 1 || gpuTempIdxStart == gpuTempIdxEnd)
                    {
                        throw new ApplicationException("vcgencmd return wrong string");
                    }

                    gpuTempRaw = gpuTempRaw.Substring(gpuTempIdxStart, gpuTempIdxEnd - gpuTempIdxStart);

                    gpuTemp = double.Parse(gpuTempRaw);
                }
                catch (Exception ex)
                {
                    ExitWithCode(ExitCode.GpuTParseErr, $"GPU temperature parse error {ex.Message}");
                }
            }
            
            
            // Show results.
            if (!showRaw)
            {
                Console.Write("Temperature: ");
            }

            if (showCpu)
            {
                if (!showRaw)
                {
                    Console.Write("CPU: {0:F1}", cpuTemp);
                }
                else
                {
                    Console.Write(cpuTemp);
                }
            }

            if (showCpu && showGpu)
            {
                Console.Write(" ");
            }

            if (showGpu)
            {
                if (!showRaw)
                {
                    Console.Write("GPU: {0:F1}", gpuTemp);
                }
                else
                {
                    Console.Write(gpuTemp);
                }
            }
            Console.WriteLine();
            
            
            ExitWithCode(ExitCode.Success);
        }

        /// <summary>
        /// Writes message if need and exits with code.
        /// </summary>
        /// <param name="code">Code.</param>
        /// <param name="message">Message.</param>
        private static void ExitWithCode(ExitCode code, string message = null)
        {
            if (message != null)
            {
                Console.WriteLine(message);
            }
            
            Environment.Exit((int)code);
        }
    }
}
