﻿using System;
using System.ServiceProcess;
using System.Timers;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Globalization;

namespace tmpctrld
{
    /// <summary>
    /// tmpctrld.
    /// </summary>
    internal class tmpctrld : ServiceBase
    {
        /// <summary></summary>
        private const string UsageStr =
            "Usage: mono-service tempctrld.exe [-h | -u | -v] addr [--debug]\r\n" +
            "Print temperature of CPU and GPU\r\n" +
            "\t-h: show usage info\r\n" +
            "\t-u: same as -h\r\n" +
            "\t-v: show version\r\n" +
            "\taddr: address of I2C target, can be in decimal or hex with 0x or 0X format\r\n" +
            "\t--debug: run in console";


        /// <summary></summary>
        private new enum ExitCode
        {
            Success = 0,
            ArgsLenErr = 1,
            ArgsErr = 2,
        }

        /// <summary>
        /// The entry point of the program, where the program control starts and ends.
        /// </summary>
        /// <param name="args">The command-line arguments.</param>
        public static void Main(String[] args)
        {
            Console.WriteLine("Main");

            if (args != null && args.Length != 0)
            {
                Console.WriteLine("Main args:");

                foreach (var arg in args)
                {
                    Console.WriteLine("\t{0}", arg);
                }
            }


            byte address = 0;

            if (args.Length == 0 || args.Length > 2)
            {
                ExitWithCode(ExitCode.ArgsLenErr, UsageStr);
            }

            var firstArg = args[0];

            if (string.IsNullOrWhiteSpace(firstArg))
            {
                ExitWithCode(ExitCode.ArgsErr, UsageStr);
            }

            if (firstArg.Length == 2)
            {
                switch (firstArg)
                {
                    case "-h":
                    case "-u":
                        ExitWithCode(ExitCode.Success, UsageStr);
                        break;

                    case "-v":
                        var assembly = AssemblyName.GetAssemblyName(Assembly.GetExecutingAssembly().Location);
                        ExitWithCode(ExitCode.Success, $"{assembly.Name} ver {assembly.Version}");
                        break;
                }
            }

            if (firstArg.Length > 2 && (firstArg.StartsWith("0x") || firstArg.StartsWith("0X")))
            {
                if (!byte.TryParse(firstArg, System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out address))
                {
                    ExitWithCode(ExitCode.ArgsErr, UsageStr);
                }
            }
            else if (!byte.TryParse(firstArg, out address))
            {
                ExitWithCode(ExitCode.ArgsErr, UsageStr);
            }


            ServiceBase.Run(new tmpctrld());
        }

        /// <summary>
        /// The timer.
        /// </summary>
        private Timer _timer = new Timer(5000) { AutoReset = false };

        /// <summary>
        /// The timer locker.
        /// </summary>
        private object _timerLocker = new object();


        private byte _address = 0;


        /// <summary>
        /// Raises the start event.
        /// </summary>
        /// <param name="args">Arguments.</param>
        protected override void OnStart(string[] args)
        {
            Console.WriteLine("OnStart");

            if (args != null && args.Length != 0)
            {
                Console.WriteLine("OnStart args:");

                foreach (var arg in args)
                {
                    Console.WriteLine("\t{0}", arg);
                }
            }


            _address = byte.Parse(args[0]);

            _timer.Elapsed += Timer_Elapsed;

            Execute();
        }

        /// <summary>
        /// Raises the stop event.
        /// </summary>
        protected override void OnStop()
        {
            Console.WriteLine("OnStop");

            lock (_timerLocker)
            {
                if (_timer != null)
                {
                    _timer.Elapsed -= Timer_Elapsed;

                    _timer.Dispose();

                    _timer = null;
                }
            }
        }

        /// <summary>
        /// Timers the elapsed.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("Timer_Elapsed");

            Execute();
        }

        /// <summary>
        /// Execute this instance.
        /// </summary>
        private void Execute()
        {
            Console.WriteLine("Execute");

            try
            {
                using (var fanControlProcess = new Process())
                {
                    fanControlProcess.StartInfo.UseShellExecute = false;
                    fanControlProcess.StartInfo.RedirectStandardOutput = true;
                    fanControlProcess.StartInfo.FileName = "mono";
                    var executePath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                    fanControlProcess.StartInfo.Arguments = $"{executePath}/fanctrl.exe {_address} -s";
                    fanControlProcess.Start();
                    fanControlProcess.WaitForExit();            

                    if (fanControlProcess.ExitCode != 0)
                    {
                        throw new ApplicationException($"fanctrl return error code {fanControlProcess.ExitCode}");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            lock (_timerLocker)
            {
                if (_timer != null)
                {
                    _timer.Start();
                }
            }
        }

        /// <summary>
        /// Writes message if need and exits with code.
        /// </summary>
        /// <param name="code">Code.</param>
        /// <param name="message">Message.</param>
        private static void ExitWithCode(ExitCode code, string message = null)
        {
            if (message != null)
            {
                Console.WriteLine(message);
            }

            Environment.Exit((int)code);
        }
    }
}
