## Group of Raspberry Pi utilites.

Clone repo at *~/_bitbucket/RPiUtils* directory:
```
git clone https://bitbucket.org/goldfynger/rpiutils ~/_bitbucket/RPiUtils
```

Update repo
```
git -C ~/_bitbucket/RPiUtils pull
```

### showtemp

**showtemp** utility shows CPU temperature

Publish and copy **showtemp** utility:
```
cd ~/_bitbucket/RPiUtils/ShowTemperature && \
dotnet publish -c Release && \
sudo mkdir /usr/sbin/showtemp && \
sudo cp -r ~/_bitbucket/RPiUtils/ShowTemperature/bin/Release/net5.0/publish/* /usr/sbin/showtemp
```

Add to *.bashrc*
```
export PATH=$PATH:/usr/sbin/showtemp
```

Apply *.bashrc*:
```
. ~/.bashrc
```

Read CPU temperature via **showtemp** utility:
```
showtemp --cpu
```

### tempctrl

**tempctrl** service controls fan via I2C bus (see https://bitbucket.org/goldfynger/rpifancontroller)

Publish and copy **tempctrl** service:
```
cd ~/_bitbucket/RPiUtils/TemperatureController && \
dotnet publish -c Release && \
sudo mkdir /usr/sbin/tempctrl && \
sudo cp -r ~/_bitbucket/RPiUtils/TemperatureController/bin/Release/net5.0/publish/* /usr/sbin/tempctrl
```

Setup **tempctrl** service:
```
sudo nano /lib/systemd/system/tempctrl.service

[Unit]   
Description=Temperature controller 
[Service]   
Type=notify   
WorkingDirectory=/usr/sbin/tempctrl   
ExecStart=/usr/sbin/tempctrl/tempctrl  
[Install]   
WantedBy=multi-user.target
```

Start and enable **tempctrl** service:
```
sudo systemctl daemon-reload
sudo systemctl status tempctrl.service
sudo systemctl start tempctrl.service
sudo systemctl status tempctrl.service
sudo systemctl enable tempctrl.service
...
sudo systemctl restart tempctrl.service
```

Show last 10 **tempctrl** service journal messages:
```
sudo journalctl -u tempctrl.service -n 10
```
