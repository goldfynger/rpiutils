using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace TemperatureController
{
    public class Worker : BackgroundService
    {
        private const int __i2cBus = 1;
        private const int __rpiFanControllerAddress = 0x30;
        private const int __pulseWidthRegisterAddress = 0xA0;
        private const int __fanStartTemperature = 50;
        private const int __fanStartPulseWidth = 40;
        private const int __fanMaxPulseWidth = 100;
        private const int __temperatureToPulseWidthMultiplier = 2;

        private static readonly TimeSpan __interval = TimeSpan.FromSeconds(10);

        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (true)
            {
                stoppingToken.ThrowIfCancellationRequested();

                try
                {
                    var rawTemperature = ReadCpuTemerature();
                    _logger.LogInformation("CPU temperature: {temperature:F1} °C", rawTemperature);

                    var temperature = Convert.ToInt32(rawTemperature);
                    int pulseWidth;

                    if (temperature < __fanStartTemperature)
                    {
                        pulseWidth = 0;
                    }
                    else
                    {
                        var diffWithStartTemperature = temperature - __fanStartTemperature;
                        var diffWithStartPulseWidth = diffWithStartTemperature * __temperatureToPulseWidthMultiplier;
                        pulseWidth = Math.Min(__fanStartPulseWidth + diffWithStartPulseWidth, __fanMaxPulseWidth);
                    }

                    _logger.LogInformation("Write pulse width {width}.", pulseWidth);
                    WriteI2C(__i2cBus, __rpiFanControllerAddress, __pulseWidthRegisterAddress, pulseWidth);
                }
                catch (Exception ex)
                {
                    _logger.LogError("Application exception: {exception}.", ex);
                }

                _logger.LogInformation("Wait for {interval} seconds.", __interval.TotalSeconds);
                await Task.Delay(__interval, stoppingToken);
            }
        }


        private static double ReadCpuTemerature()
        {
            var cpuTempRaw = string.Empty;

            try
            {
                cpuTempRaw = File.ReadAllText("/sys/class/thermal/thermal_zone0/temp");                
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"CPU temerature read exception:{Environment.NewLine}{ex.Message}", ex);
            }

            try
            {
                return double.Parse(cpuTempRaw) / 1000;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"CPU temperature parse error:{Environment.NewLine}{ex.Message}", ex);
            }
        }

        private static void WriteI2C(int i2cBus, int deviceAddress, int memoryAddress, int value)
        {
            using (var i2cSetProcess = new Process())
            {
                i2cSetProcess.StartInfo.UseShellExecute = false;
                i2cSetProcess.StartInfo.RedirectStandardOutput = true;
                i2cSetProcess.StartInfo.FileName = "i2cset"; /* If i2cset required root -> "sudo chmod 666 /dev/i2c-1" */
                i2cSetProcess.StartInfo.Arguments = $"-y {i2cBus} {deviceAddress} {memoryAddress} {value}";
                i2cSetProcess.Start();
                i2cSetProcess.StandardOutput.ReadToEnd();
                i2cSetProcess.WaitForExit();

                if (i2cSetProcess.ExitCode != 0)
                {
                    throw new ApplicationException($"i2cset return error code {i2cSetProcess.ExitCode}");
                }
            }
        }
    }
}
